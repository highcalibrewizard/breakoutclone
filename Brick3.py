from Brick import Brick
from HitDetectionBox import HitBox

class Brick3(Brick):
    """ 
    Brick1 is the base brick that only can take 1 damage
    Bricks should be 8 bricks max horizontally, and 5 bricks max vertically """

    borderWidth = 2
    brickColor = "#000000"
    brickWidth = 75
    brickheight = 20
    outlinecolor = "white"

    def __init__(self,x,y,canvas):
        self.health = 2;
        hitBox = HitBox(x,y,Brick3.brickWidth,Brick3.brickheight,self,identifier="Indestructible brick 3")
        id = canvas.create_rectangle(x,y,x+Brick3.brickWidth,y+Brick3.brickheight,fill=Brick3.brickColor, width=Brick3.borderWidth,outline=Brick3.outlinecolor)
        Brick.__init__(self,id,canvas,hitBox)

    def brickHit(self,damage = 1):
        """ Removes 1 health from brick, or optional damage=x, and returns True if brick is destroyed """
        self.health-=1
        if self.health <=0:
            self.canvas.delete(self.id)
            return True
        return False
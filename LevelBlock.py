from HitDetectionBox import HitBox

class EditorBlock(object):
    width = 75
    height = 20
    outlineW = 2
    tag = "editorBlock"

    def __init__(self,x,y,canvas, state,level,row,column,levelEditor):
        self.state = state  #current column
        self.levelEditor = levelEditor    #used to access the brickslayout
        self.level = level
        self.row = row
        self.column = column
        self.hitBox = HitBox(x,y,EditorBlock.width,EditorBlock.height,self)
        self.canvas = canvas
        self.id = canvas.create_rectangle(x,y,x+EditorBlock.width,y+EditorBlock.height,fill="",width=EditorBlock.outlineW,tag = EditorBlock.tag,outline="white")
        self.setColor()

    def setColor(self):
        if self.state == 0:
            self.canvas.itemconfig(self.id,fill="")
        elif self.state == 1:                    
            self.canvas.itemconfig(self.id,fill="green")
        elif self.state == 2:                    
            self.canvas.itemconfig(self.id,fill="blue")
        elif self.state == 3:                    
            self.canvas.itemconfig(self.id,fill="black")
        elif self.state == 4:                    
            self.canvas.itemconfig(self.id,fill="#ff3300")

    def clickBlock(self):
        if self.state < 4:
            self.state+=1
        else:
            self.state=0
        self.setColor()

        self.levelEditor.bricksLayout[self.level][self.row][self.column] = self.state


        

﻿

class HitBox(object):
    """
    A virtual hitbox for 2d graphics, will always be rectangle. You should use and update these variables when you move the object on canvas.
    You need to set objectReference in constructor for this module to work, meaning a reference to your individual canvas actors

    For example, if you are drawing and moving a paddle, you should make a class Paddle, and add in the reference self in the hitbox constructor.
    The paddle class should also keep the id supplied by canvas.create...

    The many optionals:
    identifier="name of instance" #optional name, ex: "Paddle"
    xSpeed = 0  #starting xSpeed
    ySpeed = 0  #starting ySpeed
    abs_x_speed=0   #absolute x-speed, so should be >= 0
    abs_y_speed=0   #absolute y-speed

    #These are for acceleration. Dont worry about these if you dont need them. They are NOT implemented in any methods yet
    x_acc = 0       #x acceleration, only variable is implemented in this module
    y_acc = 0       #y acceleration, only variable is implemented in this module
    abs_y_acc = 0   #absolute y acceleration. Only variable implemented
    abs_x_acc = 0   #...same for x
    accelerationTime = 0    #acceleration per "game tick". 3 would be 1 second with time.sleep(0.033). Only variable is implemented in this module
    abs_acceleration_time = 0   #absolute acceleration time. only variable declared

    Author: Benny Andersson, highcalibrewizard@gmail.com
    Version: 1.0.0

    TODO: Check screen edges is the only method that moves the coords 1 point away from impact-point. This needs to be done to prevent multiple triggering
    """

    DEBUG =True;
    SHOW_COORDS = False;


    def __init__(self,x,y,width,height,objectReference, identifier="Object", xSpeed = 0, ySpeed = 0, abs_x_speed=0, abs_y_speed=0,\
        x_acc = 0, y_acc = 0,abs_y_acc = 0, abs_x_acc = 0,accelerationTime=0,abs_acceleration_time=0):
        self.abs_acceleration_time = abs_acceleration_time
        self.abs_x_acc = abs_x_acc
        self.abs_y_acc = abs_y_acc
        self.x_acc = x_acc
        self.y_acc = y_acc
        self.accelerationTime = accelerationTime
        self.x = x;
        self.y = y;
        self.objectReference = objectReference
        self.identifier = identifier
        self.width = width
        self.height= height
        self.ySpeed = ySpeed
        self.xSpeed = xSpeed
        self.abs_x_speed  = abs_x_speed
        self.abs_y_speed = abs_y_speed

    def stop(self):
        """Sets all movement velocity to 0"""
        self.ySpeed = 0
        self.xSpeed = 0

    def coords(self,x,y):
        """Will move the hitbox to these coords"""
        self.x = x
        self.y = y

    def move(self):
        """
        Moves the hitbox, and only the hitbox. You need to call canvas.move() separately. It uses xSpeed and ySpeed, so you need to set these manually if you want
        to change current speed
        """
        if HitBox.DEBUG and HitBox.SHOW_COORDS:
            print("moving, xSpeed is %i "%self.xSpeed)
            print("moving, ySpeed is %i "%self.ySpeed)
        self.x+= self.xSpeed
        self.y+= self.ySpeed

    def place(self,x,y):
        """Places the hitbox on x,y"""
        self.x = x
        self.y = y

    def checkPointImpact(self,x,y):
        """Checking if point x,y is within self hitbox. Takes into consideration hitbox speed. Use checkImpact() if x,y is of another hitbox. Returns True or False"""

        if x >= self.x+self.xSpeed and x <= self.x+self.width+self.xSpeed:
            if y >= self.y+self.ySpeed and y <= self.y+self.height+self.ySpeed:
                if HitBox.DEBUG:
                    print("point collision: " + self.identifier + " " + str(x) + "."+str(y))
                return True
        return False

       
    def checkImpact(self,otherBox, canvas=None):
        """Checking if the current hitbox is touching another hitbox in any way. Returns boolean"""

        if canvas == None:
            if self.x+self.width >= otherBox.x and self.x <= otherBox.x+otherBox.width:
                if self.y+self.height >= otherBox.y and self.y <= otherBox.y+otherBox.height:
                    if HitBox.DEBUG:
                        print("collision between boxes: " + self.identifier + "-" + otherBox.identifier)
                    return True
            return False          
        else:
            if self.x+self.width+self.xSpeed >= otherBox.x+otherBox.xSpeed and self.x+self.xSpeed <= otherBox.x+otherBox.width+otherBox.xSpeed:
                if self.y+self.height+self.ySpeed >= otherBox.y+otherBox.ySpeed and self.y+self.ySpeed <= otherBox.y+otherBox.height+otherBox.ySpeed:
                    if HitBox.DEBUG:
                        print("collision between boxes: " + self.identifier + "-" + otherBox.identifier)
                    return True
            return False

    def checkImpactSide(self,otherBox, canvas = None):
        """returns which side self hits otherBox, if collision happens at all. Returns "LEFT"/"RIGHT"/"UP"/"DOWN or None"""
        if self.checkImpact(otherBox, canvas = canvas):
            if canvas != None:
                topPos = abs(self.y+self.ySpeed-(otherBox.y+otherBox.height+otherBox.ySpeed))
                bottomPos = abs((self.y+self.height+self.ySpeed) - (otherBox.y+otherBox.ySpeed))
                rightPos = abs ((self.x+self.width+self.xSpeed) - (otherBox.x+otherBox.xSpeed))
                leftPos = abs((self.x+self.xSpeed)-(otherBox.x+otherBox.width+otherBox.xSpeed))
            else:
                topPos = abs(self.y-(otherBox.y+otherBox.height))
                bottomPos = abs((self.y+self.height) - (otherBox.y))
                rightPos = abs ((self.x+self.width) - (otherBox.x))
                leftPos = abs((self.x)-(otherBox.x+otherBox.width))

            #TOP
            if (topPos < bottomPos and topPos < rightPos and topPos < leftPos):
                if HitBox.DEBUG:
                    print("top of " + self.identifier + " hit " + otherBox.identifier)
                if canvas == None:
                    return "TOP"
                else:
                    if topPos == 0:     #EQUAL
                        return "TOP-EQUAL"
                    else:               #OVERLAP
                        canvas.coords(self.objectReference.id,(self.x+self.xSpeed,otherBox.y+otherBox.ySpeed+otherBox.height,self.x+self.xSpeed+self.width,\
                            otherBox.y+otherBox.ySpeed+otherBox.height+self.height))       
                        self.coords(self.x+self.xSpeed,otherBox.y+otherBox.ySpeed+otherBox.height)
                        return "TOP-OVERLAP"

            #BOTTOM
            elif (bottomPos < topPos and bottomPos < rightPos and bottomPos < leftPos):
                if HitBox.DEBUG:
                    print("bottom of " + self.identifier + " hit " + otherBox.identifier)
                if canvas == None:
                    return "BOTTOM"
                else:
                    if topPos == 0:     #EQUAL
                        return "BOTTOM-EQUAL"
                    else:               #OVERLAP
                        canvas.coords(self.objectReference.id,(self.x+self.xSpeed,otherBox.y-self.height+otherBox.ySpeed,self.x+self.xSpeed+self.width,\
                            otherBox.y+otherBox.ySpeed))       
                        self.coords(self.x+self.xSpeed,otherBox.y-self.height+otherBox.ySpeed)
                        return "BOTTOM-OVERLAP"
            
            #LEFT
            elif (leftPos < bottomPos and leftPos < rightPos and leftPos < topPos):
                if HitBox.DEBUG:
                    print("left of " + self.identifier + " hit " + otherBox.identifier)
                if canvas == None:
                    return "LEFT"
                else:
                    if topPos == 0:     #EQUAL
                        return "LEFT-EQUAL"
                    else:               #OVERLAP
                        canvas.coords(self.objectReference.id,(otherBox.x+otherBox.xSpeed+otherBox.width,self.y+self.ySpeed,otherBox.x+otherBox.xSpeed+otherBox.width+self.width,\
                            self.y+self.ySpeed+self.height))       
                        self.coords(otherBox.x+otherBox.xSpeed+otherBox.width,self.y+self.ySpeed)
                        return "LEFT-OVERLAP"
            #RIGHT
            elif (rightPos < bottomPos and rightPos < topPos and rightPos < leftPos):
                if HitBox.DEBUG:
                    print("right of " + self.identifier + " hit " + otherBox.identifier)
                if canvas == None:
                    return "RIGHT"
                else:
                    if topPos == 0:     #EQUAL
                        return "RIGHT-EQUAL"
                    else:               #OVERLAP
                        canvas.coords(self.objectReference.id,(otherBox.x+otherBox.xSpeed-self.width,self.y+self.ySpeed,otherBox.x+otherBox.xSpeed,\
                            self.y+self.ySpeed+self.height))       
                        self.coords(otherBox.x+otherBox.xSpeed-self.width,self.y+self.ySpeed)
                        return "RIGHT-OVERLAP"
        else:
            return None

    def checkImpactAll(self, hitBoxArray):
        """Takes an array[], and checks for impacts with all supplied hitboxes. Returns the objectreference of the collided with object, else None
        You can combine this method with checkImpactSide()"""

        for item in hitBoxArray:
            if self.x+self.width >= item.x and self.x <= item.x+item.width:
                if self.y+self.height >= item.y and self.y <= item.y+item.height:
                    if HitBox.DEBUG:
                        print("collision between boxes: " + self.identifier + "-" + item.identifier)
                    return item.objectReference
        return None                

    def checkLeftScreen(self,otherX,canvas=None):
        """
        """
        if canvas == None:
            if self.x <= otherX:
                if HitBox.DEBUG:
                    print("left screen collision")
                return True;
            else:
                return False;
        else:
            if self.x == otherX:               #reverse speed if needed, do canvas.move
                if HitBox.DEBUG:
                    print("left screen collision")
                return "EQUAL"
            elif self.x+self.xSpeed <= otherX:      #auto align, skip doing move since it happens here, reverse speed if needed
                canvas.coords(self.objectReference.id,(otherX+1,self.y+self.ySpeed,otherX+self.width+1,self.y+self.height+self.ySpeed))
                self.coords(otherX+1,self.y+self.ySpeed)
                if HitBox.DEBUG:
                    print("left screen collision")
                return "OVERLAP";
            else:                   #do only canvas.move
                return None;

    def checkTopScreen(self,otherY,canvas=None):
        """
        """
        if canvas == None:
            if self.y <= otherY:
                if HitBox.DEBUG:
                    print("top screen collision")
                return True;
            else:
                return False;
        else:
            if self.y == otherY:               #reverse speed if needed, do canvas.move
                if HitBox.DEBUG:
                    print("top screen collision. Equal")
                return "EQUAL"
            elif self.y+self.ySpeed <= otherY:      #auto align, skip doing move since it happens here, reverse speed if needed
                canvas.coords(self.objectReference.id,(self.x+self.xSpeed,otherY+1,self.x+self.width+self.xSpeed,otherY+self.height+1))
                self.coords(self.x+self.xSpeed,otherY+1)
                if HitBox.DEBUG:
                    print("top screen collision. Overlap")
                return "OVERLAP";
            else:                   #do only canvas.move
                return None;

    def checkRightScreen(self,otherX,canvas=None):
        """
        """
        if canvas == None:
            if self.x+self.width >= otherX:
                if HitBox.DEBUG:
                    print("right screen collision")
                return True;
            else:
                return False;
        else:
            if self.x + self.width == otherX:               #reverse speed if needed, do canvas.move
                if HitBox.DEBUG:
                    print("right screen collision")
                return "EQUAL"
            elif self.x+self.xSpeed+self.width >= otherX:      #auto align, skip doing move since it happens here, reverse speed if needed
                canvas.coords(self.objectReference.id,(otherX-self.width-1,self.y+self.ySpeed,otherX-1,self.y+self.height+self.ySpeed))
                self.coords(otherX-self.width-1,self.y+self.ySpeed)
                if HitBox.DEBUG:
                    print("right screen collision")
                return "OVERLAP";
            else:                   #do only canvas.move
                return None;

    def checkBottomScreen(self,otherY,canvas=None):
        """
        """
        if canvas == None:
            if self.y+self.height >= otherY:
                if HitBox.DEBUG:
                    print("bottom screen collision")
                return True;
            else:
                return False;
        else:
            if self.y + self.height == otherY:               #reverse speed if needed, do canvas.move
                if HitBox.DEBUG:
                    print("bottom screen collision")
                return "EQUAL"
            elif self.y+self.ySpeed+self.height >= otherY:      #auto align, skip doing move since it happens here, reverse speed if needed
                canvas.coords(self.objectReference.id,(self.x+self.xSpeed,otherY-self.height-1,self.x+self.width+self.xSpeed,otherY-1))
                self.coords(self.x+self.xSpeed,otherY-self.height-1)
                if HitBox.DEBUG:
                    print("bottom screen collision")
                return "OVERLAP";
            else:                   #do only canvas.move
                return None;

    def checkAllScreenEdges(self,width,height):
        """Checks if hitbox collides with any screen-edge. Returns True if so. Does not support prediction at this time"""
        if (self.checkLeftScreen(0)):
            return True
        elif (self.checkRightScreen(width)):
            return True
        elif (self.checkTopScreen(0)):
            return True
        elif (self.checkBottomScreen(height)):
            return True
        else:
            return False


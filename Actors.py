class Actor(object):
    """This is a basic parent-class for objects using HitDetectionBox. You need to use this if you want pixel perfect collision management.
    You can easily add other things here as well, like canvas reference, x, y etc"""

    def __init__(self,id):
        self.id=id
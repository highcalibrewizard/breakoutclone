from Game import *

from Tkinter import *               #2.7
import tkFileDialog
import tkMessageBox
import tkFont

#from tkinter import *              #tkinter in >= python 3. rename tkFileDialog, tkMessageBox and tkFont to appropriate names. That is all that should be needed
#from tkinter import filedialog
#from tkinter import messagebox
#from tkinter import font 

import time
from Level import Level
from Score import ScoreBoard
import pickle
import os
from LevelBlock import EditorBlock
from custombox import MBox
import webbrowser


"""
DEPENDENCIES:
Python 2.7
Pillow (install using pip)
pygame (install using pip)
cx_freeze (install using pip)
"""

##there are global settings in Game.py as well
PADDING= 4
VERSION = "1.0.0"

def initGame(levelEditor):
    """ Starts running the actual game element"""
    run = RunGame(base,levelEditor)     #base is global
    if not run.game.error:
        base.destroyMenu()
        run.game.gameLoop()     #this must be called separately otherwise instances arent created properly (cant have a while loop in constructor)
    
def initBase():
    """This should be run first, makes the actual window"""
    return Base()      

class Base(object):
    """ This makes the root window """
    def __init__(self):
        self.makeTk()
        self.frame = None
    def makeTk(self):
        self.root = Tk()
        self.game = None
        self.root.title("PrakeOut")
        self.screen_width = self.root.winfo_screenwidth();  #monitor size
        self.screen_height = self.root.winfo_screenheight();
        #root.wm_attributes("-fullscreen",True)     #this gives fullscreen, but removes the menu
        self.root.resizable(0,0) #cant be resized
        self.root.protocol("WM_DELETE_WINDOW", self.cleanUp)
        #self.root.configure(background="Black")      #bg color of root windw
        #root.geometry("%ix%i+0+0" % (SCREEN_WIDTH, SCREEN_HEIGHT))  #maximizes screen. First two are width and height, other two are distance from left and top of screen
        #self.root.geometry("%ix%i+%i+%i" % (WIDTH,HEIGHT,self.screen_width/2-WIDTH/2,self.screen_height/2-HEIGHT/2))  #resize screen. First two are width and height, other two are distance from left and top of screen
        #root.wm_attributes("-topmost",1);   #places window in foreground
        self.makeNewGameMenu()
        self.makeCanvas()

    def cleanUp(self):
        print("quit")
        if self.game != None:   
            self.game.run = False
            self.game.stopMusic()
        self.root.destroy()
    
    def makeCanvas(self):
        self.canvas = Canvas(self.root,width=WIDTH,height=HEIGHT,bd=0,highlightthickness=0)
        self.canvas.config(bg="#737372")
        self.canvas.grid(column=1,row=0)
        #canvas.place(relx=0.5, rely=0.5, anchor=CENTER)     #centers the canvas on screen (use instead of pack)
        #canvas.itemconfig(scoreText,text="New text")   #sets new text

    def makeNewGameMenu(self):
        self.topMenu = Menu(self.root)
        self.fileMenu = Menu(self.topMenu)
        self.helpMenu = Menu(self.topMenu)
        
        self.fileMenu.add_command(label="New game", command=self.newGame)
        self.fileMenu.add_command(label="New Level editor",command=self.startLevelEditor)
        self.fileMenu.add_command(label="Quit", command=self.showQuitConfirm)

        self.helpMenu.add_command(label="About",command=self.showAbout)
        self.helpMenu.add_command(label="Instructions",command=self.showHelp)
 
        self.root.config(menu=self.topMenu)     #link topMenu to root
        self.topMenu.add_cascade(label="File",menu=self.fileMenu)   #linking cascade to topmenu
        self.topMenu.add_cascade(label="Help",menu=self.helpMenu)
    
    def showAbout(self):
        box = MBox(self.root)

        linkFont = tkFont.Font(family=FONT,underline = 1,size=9)     #need to make new font for underline

        titleLabel = Label(box.frame,text="PrakeOut, version: " + VERSION,font=(FONT,"18"))
        titleLabel.pack(pady=8,padx=8)
        genLabel = Label(box.frame,text="PrakeOut was created by Benny Andersson (High Calibre Wizard), in anno 2016\n\nhighcalibrewizard@gmail.com")
        genLabel.pack(padx=8)
        linkLabel = Label (box.frame,text="https://www.facebook.com/High-Calibre-Wizard-546088398774951/",fg="blue",cursor="hand2", font=linkFont)
        hyperlink = r"https://www.facebook.com/High-Calibre-Wizard-546088398774951/"
        linkLabel.bind("<Button-1>",lambda event, link = hyperlink: self.openBrowser(event,link))
        linkLabel.pack(padx=8)
        okButton = Button(box.frame,text="Ok",command=box.clear,width=10,relief=SOLID)
        okButton.pack(pady=8,padx=8)

    def showHelp(self):
        box = MBox(self.root)

        titleLabel = Label(box.frame,text="Instructions",font=(FONT,"18"))
        titleLabel.pack(pady=8,padx=8)
        infoLabel = Label(box.frame,text="Move paddle with LEFT and RIGHT keys\nPress SPACE to set paddle into ball catching mode, press SPACE again to release")
        infoLabel.pack(padx=8)
        okButton = Button(box.frame,text="Ok",command=box.clear,width=10,relief=SOLID)
        okButton.pack(pady=8,padx=8)

    def openBrowser(self,evt,url):
        webbrowser.open_new(url)

    def destroyMenu(self):
        self.topMenu.destroy()

    def startLevelEditor(self):
        self.destroyMenu()
        self.topMenu = Menu(self.root)
        self.fileMenu = Menu(self.topMenu)
        self.fileMenu.add_command(label="New game",command=self.newGame)
        self.fileMenu.add_command(label="New Level editor",command=self.startLevelEditor)
        self.fileMenu.add_command(label="Save levels",command=self.saveLevels)
        self.fileMenu.add_command(label = "Load levels",command=self.loadLevels)
        self.fileMenu.add_command(label="Quit",command=self.showQuitConfirm)
        self.helpMenu = Menu(self.topMenu)
        self.helpMenu.add_command(label="About",command=self.showAbout)
        self.helpMenu.add_command(label="Instructions",command=self.showHelp)
        self.root.config(menu=self.topMenu)     #link topMenu to root
        self.topMenu.add_cascade(label="File",menu=self.fileMenu)   #linking cascade to topmenu
        self.topMenu.add_cascade(label="Help",menu=self.helpMenu)
        self.canvas.delete(ALL)
        self.makeLevelEditor()
        self.addLevelEditorControls()

    def selectListBox(self,evt):
        widget=evt.widget
        selection = widget.curselection()
        print("Selected %s"%selection)
        intSelection = int(selection[0])
        if self.levelEditor.currentIndexOfLevels != intSelection:
            self.levelEditor.currentIndexOfLevels = intSelection
            self.resetBricks()
        

    def addBtnPress(self):
        print("press add")
        #self.listBox.insert("end","Level %i"%len(self.levelEditor.bricksLayout))
        if len(self.levelEditor.bricksLayout) == 0:
            self.levelEditor.addEmptyLevel()
            self.resetBricks()
            return
        self.levelEditor.addEmptyLevel()
        self.populateListBox()
        print("Size of bricksLayout is%i"%len(self.levelEditor.bricksLayout))

    def removeBtnPress(self):
        print("press remove")
        selection = self.listBox.curselection()
        if len(selection) > 0:
            if tkMessageBox.askokcancel(title="Alert!",message="Delete level?"):
                if int(selection[0]) == self.levelEditor.currentIndexOfLevels:
                    #if len(self.levelEditor.bricksLayout) == 1:     #last level
                     #   pass
                    if len(self.levelEditor.bricksLayout) >= 1:    #more levels.switch to 0
                        self.levelEditor.removeLevel(int(selection[0]))
                        self.levelEditor.currentIndexOfLevels = 0
                        self.resetBricks()
                        return

                self.levelEditor.removeLevel(int(selection[0]))
                self.populateListBox()
        else:
            tkMessageBox.showinfo(title="Notice",message="Select level to clear")
        print("Size of bricksLayout is%i"%len(self.levelEditor.bricksLayout))

    def populateListBox(self):
        self.listBox.delete(0,END)
        print("Populate listbox. Size of bricksLayout is%i"%len(self.levelEditor.bricksLayout))

        for i in range(len(self.levelEditor.bricksLayout)):
            self.listBox.insert("end","Level %i"%i)

    def addLevelEditorControls(self):
        if self.frame != None:
            self.frame.destroy()

        self.titleVar = StringVar()

        self.frame = Frame(self.root)
        self.label = Label(self.frame,text="Name of campaign")
        self.label.grid(column=0,row=0,padx=PADDING,pady=PADDING)
        self.titleInput = Entry(self.frame,textvariable = self.titleVar,relief=SOLID)
        self.titleInput.grid(column=1,row=0,padx=PADDING,pady=PADDING)
        self.listBox = Listbox(self.frame,height=4,relief=SOLID)
        self.populateListBox()
        self.listBox.bind("<Double-Button-1>",self.selectListBox)
        self.listBox.grid(column=1,row=1,padx=PADDING,pady=PADDING)
        self.addBtn = Button(self.frame,text="Add level",command=self.addBtnPress,width=10,relief=SOLID)
        self.delBtn = Button(self.frame,text="Delete level",command=self.removeBtnPress,width=10,relief=SOLID)
        self.addBtn.grid(column=0,row=1,pady=PADDING,padx=PADDING,sticky="n")
        self.delBtn.grid(column=0,row=1,pady=PADDING,padx=PADDING,sticky="s")
        self.labelGfx = Label(self.frame,text="Background")
        self.labelGfx.grid(column=0,row=2,pady=PADDING,padx=PADDING)
        self.clearGfx = Button(self.frame,text="Clear Gfx",command=self.clearBackdrop,relief=SOLID,width=10)
        self.clearGfx.grid(column=1,row=3,pady=PADDING,padx=PADDING)
        self.entryGfx = Entry(self.frame,relief=SOLID)
        self.entryGfx.bind("<Button-1>",self.fileGetGfx)
        self.entryGfx.grid(column=1,row=2,pady=PADDING,padx=PADDING)
        self.musicLabel = Label(self.frame,text="Level music")
        self.musicLabel.grid(column=0,row=4,pady=PADDING,padx=PADDING)
        self.musicEntry =Entry(self.frame,relief=SOLID)
        self.musicEntry.bind("<Button-1>",self.getMusic)
        self.musicEntry.grid(column=1,row=4,pady=PADDING,padx=PADDING)
        self.clearMusic = Button(self.frame,text="Clear music",command=self.removeLevelMusic,relief=SOLID,width=10)
        self.clearMusic.grid(column=1,row=5,pady=PADDING,padx=PADDING)
        self.frame.grid(column=0,row=0,padx=PADDING,pady=PADDING,sticky="n")

    def removeLevelMusic(self):
        self.musicEntry.delete(0,END)
        self.levelEditor.removeMusic(self.levelEditor.currentIndexOfLevels)
    
    def getMusic(self,evt):
        if not os.path.isdir("./campaigns"):
            os.makedirs("./campaigns")
        fileName = tkFileDialog.askopenfilename(filetypes=[("MP3-file","*.mp3"),("OGG-file","*.ogg")],initialdir="./campaigns",title="Select level music")
        fileName = os.path.split(fileName)
        if fileName[1] != "":
            self.levelEditor.levelMusic[self.levelEditor.currentIndexOfLevels]=fileName[1]
            self.musicEntry.delete(0,END)
            self.musicEntry.insert(0,fileName[1])
    
    def clearBackdrop(self):
        self.entryGfx.delete(0,END)
        self.levelEditor.removeBg(self.levelEditor.currentIndexOfLevels)

    def fileGetGfx(self,evt):
        print("grabbing gfx")
        if not os.path.isdir("./campaigns"):
            os.makedirs("./campaigns")
        fileName = tkFileDialog.askopenfilename(filetypes=[("PNG file","*.png"),("JPG file","*.jpg")],initialdir="./campaigns",title="Select level background")
        fileName = os.path.split(fileName)
        if fileName[1] != "":
            self.levelEditor.levelPictures[self.levelEditor.currentIndexOfLevels]=fileName[1]
            self.entryGfx.delete(0,END)
            self.entryGfx.insert(0,fileName[1])

    def showQuitConfirm(self):
        self.root.destroy()

    def saveLevels(self):
        print("saving campaign")
        self.levelEditor.gameName = self.titleVar.get()

        if self.levelEditor.gameName == "":
            tkMessageBox.showerror(title="Error!", message="You need to specify campaign name. A folder for your project will be autogenerated")
            return
        print(self.levelEditor.gameName)
        mypath = "./campaigns/"+self.levelEditor.gameName+"/"
        if not os.path.isdir(mypath):
            os.makedirs(mypath)
        fileName=tkFileDialog.asksaveasfilename(initialdir=mypath,title="save campaign file",filetypes=[("campaign data file","*.dat")])
        print(fileName)

        if fileName != "":
            try:
                if fileName.find(".dat",((len(fileName)-1)-4)) == -1:
                    fileName+=".dat"
                   
                pickle.dump(self.levelEditor,open(fileName,"wb"),protocol=2)
                tkMessageBox.showinfo(title="Notice",message="Campaign saved")
            except:
                if tkMessageBox.askretrycancel(title="Error",message="Failure saving file"):
                    self.saveLevels()


    def loadLevels(self):

        mypath = "./campaigns/"
        if not os.path.isdir(mypath):
            os.makedirs(mypath)

        path = tkFileDialog.askopenfilename(initialdir="./campaign",title="choose campaign file",filetypes=[("campaign data file","*.dat")])

        if path != "":
            self.canvas.delete(EditorBlock.tag)
            self.currentLevelBricks = []
            print("loading campaign")
            try:
                self.levelEditor = pickle.load(open(path,"rb"))
                print("Loading. Size of bricksLayout is%i"%len(self.levelEditor.bricksLayout))
                self.levelEditor.currentIndexOfLevels = 0
                self.resetBricks()
                self.titleInput.delete(0,END)
                self.titleInput.insert(0,self.levelEditor.gameName)
                self.titleVar.set(self.levelEditor.gameName)

            except:
                if tkMessageBox.askretrycancel(title="Error",message="Failure loading file"):
                    self.loadLevels()




    def detectEditorClick(self,evt):
        print("detected click @%i,%i"%(evt.x,evt.y))
        for item in range(len(self.currentLevelBricks)):
            if self.currentLevelBricks[item].hitBox.checkPointImpact(evt.x,evt.y):
                print("clicked brick")
                self.currentLevelBricks[item].clickBlock()
                break


    def makeLevelEditor(self):
        print("Making level editor")
        self.levelEditor = Level()
        self.currentLevelBricks = []        #the current graphical representation of the bricks in the level editor
        self.makeLevelEditorBricks(self.levelEditor.bricksLayout,self.levelEditor.currentIndexOfLevels)
        if len(self.levelEditor.bricksLayout) > 0:
            self.canvas.create_text(WIDTH/2,HEIGHT/2,text="Editor\nLevel: %i"%self.levelEditor.currentIndexOfLevels,anchor="center",font=(FONT,32),justify="center")
        else:
            self.canvas.create_text(WIDTH/2,HEIGHT/2,text="Editor",anchor="center",font=(FONT,32),justify="center")
        self.canvas.bind("<Button-1>",self.detectEditorClick)

    def resetBricks(self):
        """this is ran when switching level in leveleditor"""
        self.currentLevelBricks = []        #the current graphical representation of the bricks in the level editor
        self.canvas.delete(ALL)
        self.makeLevelEditorBricks(self.levelEditor.bricksLayout,self.levelEditor.currentIndexOfLevels)
        self.populateListBox()
        if len(self.levelEditor.bricksLayout) > 0:
            self.canvas.create_text(WIDTH/2,HEIGHT/2,text="Editor\nLevel: %i"%self.levelEditor.currentIndexOfLevels,anchor="center",font=(FONT,32),justify="center")
        else:
            self.canvas.create_text(WIDTH/2,HEIGHT/2,text="Editor",anchor="center",font=(FONT,32),justify="center")
        self.musicEntry.delete(0,END)
        self.musicEntry.insert(0,self.levelEditor.levelMusic[self.levelEditor.currentIndexOfLevels])

        self.entryGfx.delete(0,END)
        self.entryGfx.insert(0,self.levelEditor.levelPictures[self.levelEditor.currentIndexOfLevels])

    def makeLevelEditorBricks(self,bricksLayout,currentLevel):
        """ The bricks layout is described as 3 dimensional array where indexes are level,row,column """

        if len(bricksLayout) == 0:
            return

        for row in range(len(bricksLayout[currentLevel])):
            for column in range(len(bricksLayout[currentLevel][row])):
                if bricksLayout[currentLevel][row][column] == 1:             #regular brick
                    self.currentLevelBricks.append(EditorBlock(Brick1.brickWidth*column,DISTANCE_TO_FIRST_BRICK+(Brick1.brickheight*row),self.canvas,1,currentLevel,row,column,self.levelEditor))
                elif bricksLayout[currentLevel][row][column]== 2:             #tough brick
                    self.currentLevelBricks.append(EditorBlock(Brick1.brickWidth*column,DISTANCE_TO_FIRST_BRICK+(Brick1.brickheight*row),self.canvas,2,currentLevel,row,column,self.levelEditor))
                elif bricksLayout[currentLevel][row][column] == 0:             #empty space
                    self.currentLevelBricks.append(EditorBlock(Brick1.brickWidth*column,DISTANCE_TO_FIRST_BRICK+(Brick1.brickheight*row),self.canvas,0,currentLevel,row,column,self.levelEditor))
                elif bricksLayout[currentLevel][row][column] == 3:            #indestructible
                    self.currentLevelBricks.append(EditorBlock(Brick1.brickWidth*column,DISTANCE_TO_FIRST_BRICK+(Brick1.brickheight*row),self.canvas,3,currentLevel,row,column,self.levelEditor))
                elif bricksLayout[currentLevel][row][column] == 4:            #win brick
                    self.currentLevelBricks.append(EditorBlock(Brick1.brickWidth*column,DISTANCE_TO_FIRST_BRICK+(Brick1.brickheight*row),self.canvas,4,currentLevel,row,column,self.levelEditor))
        print("Num editor bricks created" + str(len(self.currentLevelBricks)))
        
        
    def newGame(self):
        mypath = "./campaigns/"
        if not os.path.isdir(mypath):
            os.makedirs(mypath)
        path = tkFileDialog.askopenfilename(initialdir="./campaign",title="choose campaign file",filetypes=[("campaign data file","*.dat")])
        
        if path != "":
            try:
                self.levelEditor = pickle.load(open(path,'rb'))
                self.root.unbind("<Button-1>",funcid = None)
                self.canvas.delete(ALL)
                if self.frame != None:
                    self.frame.destroy()
            except:
                if tkMessageBox.askretrycancel(title="Error!",message="Not a valid campaign file"):
                    self.newGame()   
            initGame(self.levelEditor)

            

class RunGame():

    def __init__(self,base, levelEditor):
        self.root = base.root
        self.base = base
        self.getLayout(levelEditor)
        self.totalTime = 0
        self.currentLevel = 0
        self.game = GameInstance(self.bricksLayout,self)
        self.base.game = self.game

    def displayError(self,errorMess):
        messagebox.showerror(title="Error!",message=errorMess)

    def makeNewLevel(self):
        """ This is run for next stage """
        base.canvas.delete(ALL)
        self.game = GameInstance(self.bricksLayout,self)
        self.base.game = self.game
        if not self.game.error:
            self.game.gameLoop()
        else:
            self.base.makeNewGameMenu()


    def getLayout(self,levelEditor):
        self.levelEditor = levelEditor
        self.bricksLayout = levelEditor.bricksLayout
        self.layoutName = levelEditor.gameName

    def gameOver(self):
        self.game.stopMusic()
        self.game.run = False
        self.game.canvas.delete(ALL)
        self.game.canvas.create_text(WIDTH/2,HEIGHT/2,anchor="center",font=(FONT,32),fill="blue",text="Game over!")
        self.base.makeNewGameMenu()

    def displayScoreBoard(self):
        print("pickling")
        self.game.canvas.delete(self.gameWonMess)
        scoreList = []

        try:
            savedScore = pickle.load(open("scr.dat","rb"))
            scoreList = savedScore.sortList(self.layoutName,self.totalTime)
            print("in old file " + str(savedScore.scoreList))
            pickle.dump(savedScore,open("scr.dat","wb"),protocol=2)
        except:
            newSavedScore = ScoreBoard()
            scoreList = newSavedScore.sortList(self.layoutName,self.totalTime)
            print("in new file " + str(newSavedScore.scoreList))
            pickle.dump(newSavedScore,open("scr.dat","wb"))
        
        self.game.canvas.create_text(WIDTH/2,16,text=self.layoutName,font=(FONT,32),fill="blue",anchor="n")
        for score in range(len(scoreList)):
            if score < 9:
                self.game.canvas.create_text(WIDTH/2,64+((32+16)*score),text="%.1f"%scoreList[score],font=(FONT,32),anchor="n")
            else:
                break
        self.base.makeNewGameMenu()

    def switchLevel(self):
        self.game.stopMusic()
        print("switching level")
        self.currentLevel+=1
        self.game.run = False
        self.game.canvas.delete(ALL)
        self.totalTime+= self.game.levelTime


        if self.currentLevel < len(self.bricksLayout):
            print("prepping next lvl")
            self.game.canvas.create_text(WIDTH/2,HEIGHT/2,anchor="center",text="Stage won!",font=(FONT, 32),fill="blue")
            self.root.after(2000,self.makeNewLevel)       #timer
        else:
            print("final level done")
            self.gameWonMess = self.game.canvas.create_text(WIDTH/2,HEIGHT/2,anchor="center",text="Game won! \nTotal time: %.1fs"%self.totalTime,font=(FONT, 32),fill="blue",justify="center")
            self.root.after(2000,self.displayScoreBoard)


base = initBase()
base.root.mainloop()
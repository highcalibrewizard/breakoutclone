﻿from HitDetectionBox import *
import random
from Actors import Actor
from Brick1 import *
from Brick2 import *
from Brick3 import Brick3
from Brick4 import Brick4
import wave
from pygame import mixer



class MakeBall(Actor):

    BALL_WIDTH = 25     #should be 25
    BALL_HEIGHT = 25
    BALL_SPEED_X=4    #4
    BALL_SPEED_Y=13     #13
        
    def getRandomXSpeed(self):
        randomX = [MakeBall.BALL_SPEED_X,-MakeBall.BALL_SPEED_X]          #starting X-speed. should be 10,-10
        random.shuffle(randomX)
        return randomX[0]

    def playRelease(self):
        self.sRelease.play()

    def __init__(self,canvas,color,x,y, screen_width,screen_height,topBorder,paddle, bricks,gamehandler):
        self.totalTime = 0
        self.canvas = canvas
        self.color = color
        self.bricks = bricks
        self.gamehandler = gamehandler
        self.topBorder = topBorder
        self.paddle = paddle
        self.state = "caught" #if ball is free or caught
        self.screen_height = screen_height
        self.screen_width=screen_width
        self.hitbox = HitBox(x,y,MakeBall.BALL_WIDTH,MakeBall.BALL_HEIGHT,self,abs_x_speed=MakeBall.BALL_SPEED_X, abs_y_speed=MakeBall.BALL_SPEED_Y, ySpeed=0,xSpeed=0,\
            identifier="Ball")
        Actor.__init__(self,canvas.create_oval(self.hitbox.x,self.hitbox.y,self.hitbox.width+self.hitbox.x,self.hitbox.height+self.hitbox.y,fill=self.color, width=2,outline="#33cc33"))
        mixer.init()
        self.sPling = mixer.Sound('pling.wav')
        self.sWall = mixer.Sound("wall.wav")
        self.sGameOver = mixer.Sound("gameover.wav")
        self.sPaddle = mixer.Sound("paddle.wav")
        self.sRelease = mixer.Sound("launch.wav")

    
    def playSoundAndRemoveBrick(self,currentBrick):
        if isinstance(currentBrick,Brick1):
            self.sPling.play()
            if currentBrick.brickHit():
                self.bricks.remove(currentBrick)
        elif isinstance(currentBrick,Brick2):
            self.sPling.play()
            if currentBrick.brickHit():
                self.bricks.remove(currentBrick)
        elif isinstance(currentBrick,Brick3):
            self.sPling.play()
        elif isinstance(currentBrick,Brick4):
            self.sPling.play()
            if currentBrick.brickHit():
                self.bricks.remove(currentBrick)
                self.gamehandler.switchLevel()
            
        

    def draw(self):
        ###BRICKS###
        #checking bricks
        for brick in range(len(self.bricks)):
            checkBrick = self.hitbox.checkImpactSide(self.bricks[brick].hitbox,canvas = self.canvas)

            if checkBrick == None:
                continue
            elif checkBrick == "BOTTOM-EQUAL":
                self.playSoundAndRemoveBrick(self.bricks[brick])
                self.hitbox.ySpeed = -self.hitbox.abs_y_speed
                self.hitbox.move()
                self.canvas.move(self.id,self.hitbox.xSpeed,self.hitbox.ySpeed)
                return
            elif checkBrick == "BOTTOM-OVERLAP":
                self.playSoundAndRemoveBrick(self.bricks[brick])
                self.hitbox.ySpeed = -self.hitbox.abs_y_speed
                return
            elif checkBrick == "TOP-EQUAL":
                self.playSoundAndRemoveBrick(self.bricks[brick])
                self.hitbox.ySpeed = self.hitbox.abs_y_speed
                self.hitbox.move()
                self.canvas.move(self.id,self.hitbox.xSpeed,self.hitbox.ySpeed)
                return
            elif checkBrick == "TOP-OVERLAP":
                self.playSoundAndRemoveBrick(self.bricks[brick])
                self.hitbox.ySpeed = self.hitbox.abs_y_speed
                return
            elif checkBrick == "LEFT-EQUAL":
                self.playSoundAndRemoveBrick(self.bricks[brick])
                self.hitbox.xSpeed = self.hitbox.abs_x_speed
                self.hitbox.move()
                self.canvas.move(self.id,self.hitbox.xSpeed,self.hitbox.ySpeed)
                return
            elif checkBrick == "LEFT-OVERLAP":
                self.playSoundAndRemoveBrick(self.bricks[brick])
                self.hitbox.xSpeed = self.hitbox.abs_x_speed
                return
            elif checkBrick == "RIGHT-EQUAL":
                self.playSoundAndRemoveBrick(self.bricks[brick])
                self.hitbox.xSpeed = -self.hitbox.abs_x_speed
                self.hitbox.move()
                self.canvas.move(self.id,self.hitbox.xSpeed,self.hitbox.ySpeed)
                return
            elif checkBrick == "RIGHT-OVERLAP":
                self.playSoundAndRemoveBrick(self.bricks[brick])
                self.hitbox.xSpeed = -self.hitbox.abs_x_speed
                return
        
        ###PADDLE###
        #Checking collison with paddle
        side = self.hitbox.checkImpactSide(self.paddle.hitbox, canvas=self.canvas)
        if (side == "BOTTOM-EQUAL"):
            self.sPaddle.play()
            if self.paddle.state=="catch":
                print("ball caught")
                self.state = "caught"
                self.hitbox.coords(self.paddle.hitbox.x+self.paddle.hitbox.width/2-self.hitbox.width/2,self.paddle.hitbox.y-self.hitbox.height-1)
                self.canvas.coords(self.id,(self.paddle.hitbox.x+self.paddle.hitbox.width/2-self.hitbox.width/2,self.paddle.hitbox.y-self.hitbox.height-1,\
                    self.paddle.hitbox.x+self.paddle.hitbox.width/2-self.hitbox.width/2+self.hitbox.width,self.paddle.hitbox.y-1))
                self.hitbox.xSpeed = 0
                self.hitbox.ySpeed = 0
            else:
                self.hitbox.ySpeed = -self.hitbox.abs_y_speed
                self.hitbox.move()
                self.canvas.move(self.id,self.hitbox.xSpeed,self.hitbox.ySpeed)
            return
        elif (side == "BOTTOM-OVERLAP"):
            self.sPaddle.play()
            if self.paddle.state=="catch":
                print("ball caught")
                self.state = "caught"
                self.hitbox.coords(self.paddle.hitbox.x+self.paddle.hitbox.width/2-self.hitbox.width/2,self.paddle.hitbox.y-self.hitbox.height-1)
                self.canvas.coords(self.id,(self.paddle.hitbox.x+self.paddle.hitbox.width/2-self.hitbox.width/2,self.paddle.hitbox.y-self.hitbox.height-1,\
                    self.paddle.hitbox.x+self.paddle.hitbox.width/2-self.hitbox.width/2+self.hitbox.width,self.paddle.hitbox.y-1))
                self.hitbox.xSpeed = 0
                self.hitbox.ySpeed = 0
            else:
                self.hitbox.ySpeed = -self.hitbox.abs_y_speed
            return
        elif (side == "TOP-EQUAL"):
            self.sPaddle.play()
            self.hitbox.ySpeed = self.hitbox.abs_y_speed
            self.hitbox.move()
            self.canvas.move(self.id,self.hitbox.xSpeed,self.hitbox.ySpeed)
            return
        elif (side == "TOP-OVERLAP"):
            self.sPaddle.play()
            self.hitbox.ySpeed = self.hitbox.abs_y_speed
            return
        elif (side == "LEFT-EQUAL"):
            self.sPaddle.play()
            if self.hitbox.xSpeed>0 and self.hitbox.abs_y_speed > 0:
                self.hitbox.ySpeed = -self.hitbox.abs_y_speed
            elif self.hitbox.xSpeed<0 and self.hitbox.abs_y_speed > 0:
                pass
            elif self.hitbox.xSpeed<0 and self.hitbox.abs_y_speed < 0:
                pass
            elif self.hitbox.xSpeed>0 and self.hitbox.abs_y_speed < 0:
                self.hitbox.ySpeed = self.hitbox.abs_y_speed

            self.hitbox.xSpeed = self.hitbox.abs_x_speed
            self.hitbox.move()
            self.canvas.move(self.id,self.hitbox.xSpeed,self.hitbox.ySpeed)
            return
        elif (side == "LEFT-OVERLAP"):
            self.sPaddle.play()
            if self.hitbox.xSpeed>0 and self.hitbox.abs_y_speed > 0:
                self.hitbox.ySpeed = -self.hitbox.abs_y_speed
            elif self.hitbox.xSpeed<0 and self.hitbox.abs_y_speed > 0:
                pass
            elif self.hitbox.xSpeed<0 and self.hitbox.abs_y_speed < 0:
                pass
            elif self.hitbox.xSpeed>0 and self.hitbox.abs_y_speed < 0:
                self.hitbox.ySpeed = self.hitbox.abs_y_speed
            self.hitbox.xSpeed = self.hitbox.abs_x_speed
            return
        elif (side == "RIGHT-EQUAL"):
            self.sPaddle.play()
            if self.hitbox.xSpeed>0 and self.hitbox.abs_y_speed < 0:
                pass
            elif self.hitbox.xSpeed<0 and self.hitbox.abs_y_speed < 0:
                self.hitbox.ySpeed = self.hitbox.abs_y_speed
            elif self.hitbox.xSpeed<0 and self.hitbox.abs_y_speed > 0:
                self.hitbox.ySpeed = -self.hitbox.abs_y_speed
            elif self.hitbox.xSpeed>0 and self.hitbox.abs_y_speed > 0:
                pass
            self.hitbox.xSpeed = -self.hitbox.abs_x_speed
            self.hitbox.move()
            self.canvas.move(self.id,self.hitbox.xSpeed,self.hitbox.ySpeed)
            return
        elif (side == "RIGHT-OVERLAP"):
            self.sPaddle.play()
            if self.hitbox.xSpeed>0 and self.hitbox.abs_y_speed < 0:
                pass
            elif self.hitbox.xSpeed<0 and self.hitbox.abs_y_speed < 0:
                self.hitbox.ySpeed = self.hitbox.abs_y_speed
            elif self.hitbox.xSpeed<0 and self.hitbox.abs_y_speed > 0:
                self.hitbox.ySpeed = -self.hitbox.abs_y_speed
            elif self.hitbox.xSpeed>0 and self.hitbox.abs_y_speed > 0:
                pass
            self.hitbox.xSpeed = -self.hitbox.abs_x_speed
            return    

        ###SCREEN EDGES###
        #Checking collision with left side of screen
        checkLeft = self.hitbox.checkLeftScreen(0, canvas=self.canvas)

        if checkLeft == "EQUAL":
            self.sWall.play()
            self.hitbox.xSpeed = self.hitbox.abs_x_speed;
            self.hitbox.move()
            self.canvas.move(self.id, self.hitbox.xSpeed,self.hitbox.ySpeed)
            return
        elif checkLeft == "OVERLAP":
            self.sWall.play()
            self.hitbox.xSpeed = self.hitbox.abs_x_speed;
            return

        #checking right side collision
        checkRight = self.hitbox.checkRightScreen(self.screen_width,canvas=self.canvas)

        if checkRight == "EQUAL":
            self.sWall.play()
            self.hitbox.xSpeed = -self.hitbox.abs_x_speed;
            self.hitbox.move()
            self.canvas.move(self.id, self.hitbox.xSpeed,self.hitbox.ySpeed)
            return
        elif checkRight == "OVERLAP":
            self.sWall.play()
            self.hitbox.xSpeed = -self.hitbox.abs_x_speed;
            return
        
        #checking top side collision
        checkTop = self.hitbox.checkTopScreen(self.topBorder,canvas=self.canvas)

        if checkTop == "EQUAL":
            self.sWall.play()
            self.hitbox.ySpeed = self.hitbox.abs_y_speed;
            self.hitbox.move()
            self.canvas.move(self.id, self.hitbox.xSpeed,self.hitbox.ySpeed)
            return
        elif checkTop == "OVERLAP":
            self.sWall.play()
            self.hitbox.ySpeed = self.hitbox.abs_y_speed;
            return

        #checking bottom side collision
        checkBottom = self.hitbox.checkBottomScreen(self.screen_height,canvas=self.canvas)

        if checkBottom == "EQUAL":
            self.sGameOver.play()
            self.gamehandler.gameOver()
        elif checkBottom == "OVERLAP":
            self.sGameOver.play()
            self.gamehandler.gameOver()



        #If no collision detected move as usual
        if self.state == "free":
            self.hitbox.move()
            self.canvas.move(self.id,self.hitbox.xSpeed,self.hitbox.ySpeed)


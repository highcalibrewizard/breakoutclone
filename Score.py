
class ScoreBoard(object):

    def __init__(self):
        self.scoreList = {}     #dict, each key holds an array of scores
        print("in scoreboard init")

    def sortList(self,keyName,newScore):
        self.tempList = self.scoreList.get(keyName)
        newScoreList = [newScore]

        if self.tempList == None:
            print("No list found")
            self.scoreList[keyName]=newScoreList
            print("in scoreboard" + str(self.scoreList))
            return self.scoreList[keyName]
        else:
            print("list found")
            self.tempList.append(newScore)
            self.tempList.sort()
            print("in scoreboard" + str(self.scoreList))
            return self.tempList
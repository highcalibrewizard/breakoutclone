from Brick import Brick
from HitDetectionBox import HitBox

class Brick2(Brick):
    """ 
    Brick1 is the base brick that only can take 1 damage
    Bricks should be 8 bricks max horizontally, and 5 bricks max vertically """

    borderWidth = 2
    brickColor = "blue"
    brickWidth = 75
    outlinecolor = "white"
    brickheight = 20

    def __init__(self,x,y,canvas):
        self.health = 2;
        hitBox = HitBox(x,y,Brick2.brickWidth,Brick2.brickheight,self,identifier="Brick2")
        id = canvas.create_rectangle(x,y,x+Brick2.brickWidth,y+Brick2.brickheight,fill=Brick2.brickColor, width=Brick2.borderWidth,outline=Brick2.outlinecolor)
        Brick.__init__(self,id,canvas,hitBox)

    def brickHit(self,damage = 1):
        """ Removes 1 health from brick, or optional damage=x, and returns True if brick is destroyed """
        self.health-=1
        if self.health <=0:
            self.canvas.delete(self.id)
            return True
        self.canvas.itemconfig(self.id,fill="green")
        return False
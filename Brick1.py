from Brick import Brick
from HitDetectionBox import HitBox

class Brick1(Brick):
    """ 
    Brick1 is the base brick that only can take 1 damage
    Bricks should be 8 bricks max horizontally, and 5 bricks max vertically """

    borderWidth = 2
    brickColor = "green"
    outlinecolor = "white"
    brickWidth = 75
    brickheight = 20

    def __init__(self,x,y,canvas):
        self.health = 1;
        hitBox = HitBox(x,y,Brick1.brickWidth,Brick1.brickheight,self,identifier="Brick1")
        id = canvas.create_rectangle(x,y,x+Brick1.brickWidth,y+Brick1.brickheight,fill=Brick1.brickColor, width=Brick1.borderWidth,outline=Brick1.outlinecolor)
        Brick.__init__(self,id,canvas,hitBox)

    def brickHit(self,damage = 1):
        """ Removes 1 health from brick, or optional damage=x, and returns True if brick is destroyed """
        self.health-=1
        if self.health <=0:
            self.canvas.delete(self.id)
            return True
        return False

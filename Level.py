import copy


class Level(object):
   """ This class holds a constructed PrakeOut-campaign """

   levelPlaceHolder =[
                        [0,0,0,0,0,0,0,0],
                        [0,0,0,0,0,0,0,0],
                        [0,0,0,0,0,0,0,0],
                        [0,0,0,0,0,0,0,0],
                        [0,0,0,0,0,0,0,0],
                        [0,0,0,0,0,0,0,0],
                        [0,0,0,0,0,0,0,0],
                        [0,0,0,0,0,0,0,0]
                    ]

   def addEmptyLevel(self):
       """Adds in empty level"""
       self.bricksLayout.append(copy.deepcopy(Level.levelPlaceHolder))
       self.levelPictures.append("")
       self.levelMusic.append("")

   def removeLevel(self,levelToRemove):
       del self.bricksLayout[levelToRemove]
       del self.levelPictures[levelToRemove]
       del self.levelMusic[levelToRemove]

   def removeBg(self,bgToRemove):
        self.levelPictures[bgToRemove]=""

   def removeMusic(self,musicToRemove):
        self.levelMusic[musicToRemove]=""

   def __init__(self):
       self.currentIndexOfLevels = 0
       self.levelMusic=[]
       self.levelPictures=[]            #empty levelpictures should be "" on empty, or "/.campaigns/graphicfile"
       self.gameName = "PrakeOut"       #name of the campaign,also savefile-name
       self.bricksLayout = []           #a layout of bricks. 3d array with each level
       self.addEmptyLevel()
          
       


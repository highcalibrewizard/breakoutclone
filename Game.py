﻿from Tkinter import *
from HitDetectionBox import *
from Paddle import *
from Ball import MakeBall
from Brick1 import Brick1
from Brick2 import Brick2
from Brick3 import Brick3
from Brick4 import Brick4
import time
from PIL import Image,ImageTk
from pygame import mixer_music
import pygame.mixer



#Settings
FONT = "Helvetica"
WIDTH = 600;        #canvas size
HEIGHT = 800;
DISTANCE_TO_FIRST_BRICK = 60;       #making room for score and other text
PADDLE_COLOR = "blue"
BALL_COLOR = "green"
UPDATE_TIME = 0.033



class GameInstance():

    def stopMusic(self):
        mixer_music.stop()

    def __init__(self, bricksLayout,gameHandler):
        self.bricksLayout = bricksLayout
        self.gameHandler = gameHandler
        self.error = False
        self.showHelp = False
        self.run = True     #game loop
        self.levelTime = 0
        self.canvas = gameHandler.base.canvas

        if self.gameHandler.levelEditor.levelPictures[self.gameHandler.currentLevel] != "":
            try:
                self.currImg = Image.open("./campaigns/"+self.gameHandler.levelEditor.gameName+"/"+self.gameHandler.levelEditor.levelPictures[self.gameHandler.currentLevel])
                resized = self.currImg.resize((WIDTH,HEIGHT),0)
                self.phtImg = ImageTk.PhotoImage(resized)
                self.backDrop = self.canvas.create_image(0,0,anchor="nw",image=self.phtImg)
            except:
                self.gameHandler.displayError("Error loading background. Background needs to be in same directory as *.dat")
                self.error = True
        else:
            self.backDrop = None
            self.phtImg = None

        if self.gameHandler.levelEditor.levelMusic[self.gameHandler.currentLevel] != "":
            try:
                pygame.mixer.init(44100,-16,2, 1024)
                mixer_music.load("./campaigns/"+self.gameHandler.levelEditor.gameName+"/"+self.gameHandler.levelEditor.levelMusic[self.gameHandler.currentLevel])
                mixer_music.play(-1)    #-1 is looping
            except:
                self.gameHandler.displayError("Error loading music. Music needs to be in same directory as *.dat")
                self.error = True
             
        if not self.error:
            self.brickList = []    #the list of all da created bricks
            self.makeBricks()
            self.helpMess = self.canvas.create_text(WIDTH/2,HEIGHT/2,anchor="center",text="Space to launch",fill="green",font=(FONT,32))
            self.makeBallAndPaddle()
            self.scoreText = self.canvas.create_text(0,0,text="Time :", font=(FONT,32),anchor="nw",fill="green")
            self.levelName = self.canvas.create_text(WIDTH,0,font=(FONT, 32),fill="cyan",anchor="ne",text="Level: "+str(self.gameHandler.currentLevel+1))


    
    def gameLoop(self):
        while self.run:
            self.doGame()

    def makeBricks(self):
        """ The bricks layout is described as 3 dimensional array where indexes are level,row,column """

        for row in range(len(self.bricksLayout[self.gameHandler.currentLevel])):
            for column in range(len(self.bricksLayout[self.gameHandler.currentLevel][row])):
                if self.bricksLayout[self.gameHandler.currentLevel][row][column] == 1:             #regular brick
                    self.brickList.append(Brick1(Brick1.brickWidth*column,DISTANCE_TO_FIRST_BRICK+(Brick1.brickheight*row),self.canvas))
                elif self.bricksLayout[self.gameHandler.currentLevel][row][column]== 2:             #tough brick
                    self.brickList.append(Brick2(Brick2.brickWidth*column,DISTANCE_TO_FIRST_BRICK+(Brick2.brickheight*row),self.canvas))
                elif self.bricksLayout[self.gameHandler.currentLevel][row][column] == 0:             #empty space
                    pass
                elif self.bricksLayout[self.gameHandler.currentLevel][row][column] == 3:            #indestructible
                    self.brickList.append(Brick3(Brick3.brickWidth*column,DISTANCE_TO_FIRST_BRICK+(Brick3.brickheight*row),self.canvas))
                elif self.bricksLayout[self.gameHandler.currentLevel][row][column] == 4:            #win brick
                    self.brickList.append(Brick4(Brick4.brickWidth*column,DISTANCE_TO_FIRST_BRICK+(Brick4.brickheight*row),self.canvas))
        print("Num bricks created" + str(len(self.brickList)))

    def makeBallAndPaddle(self):
        self.paddle = MakePaddle(self.canvas,PADDLE_COLOR,WIDTH/2-MakePaddle.PADDLE_WIDTH/2,HEIGHT - MakePaddle.PADDLE_HEIGHT*2, WIDTH,HEIGHT,self)
        #self.ball = MakeBall(self.canvas,BALL_COLOR,WIDTH/2-MakeBall.BALL_WIDTH/2,HEIGHT/2-MakeBall.BALL_HEIGHT/2,WIDTH,HEIGHT,DISTANCE_TO_FIRST_BRICK,self.paddle,self.brickList,self.gameHandler)
        self.ball = MakeBall(self.canvas,BALL_COLOR,self.paddle.hitbox.x+self.paddle.hitbox.width/2-MakeBall.BALL_WIDTH/2,self.paddle.hitbox.y-1-MakeBall.BALL_HEIGHT,\
                             WIDTH,HEIGHT,DISTANCE_TO_FIRST_BRICK,self.paddle,self.brickList,self.gameHandler)
    
    def releaseBall(self):
        if self.ball.state == "caught":
            print("releasing ball")
            self.ball.playRelease()
            self.ball.state = "free"
            self.ball.hitbox.xSpeed = self.ball.getRandomXSpeed()
            self.ball.hitbox.ySpeed = -MakeBall.BALL_SPEED_Y
            if self.showHelp == False:
                self.showHelp = True
                self.canvas.delete(self.helpMess)

    def moveBallWithPaddle(self):
        if self.ball.state == "caught":
            self.ball.hitbox.coords(self.paddle.hitbox.x+self.paddle.hitbox.width/2-self.ball.hitbox.width/2,self.paddle.hitbox.y-self.ball.hitbox.height-1)
            self.canvas.coords(self.ball.id,(self.paddle.hitbox.x+self.paddle.hitbox.width/2-self.ball.hitbox.width/2,self.paddle.hitbox.y-self.ball.hitbox.height-1,\
                self.paddle.hitbox.x+self.paddle.hitbox.width/2-self.ball.hitbox.width/2+self.ball.hitbox.width,self.paddle.hitbox.y-1))

    def doGame(self):
        self.ball.draw()
        self.gameHandler.root.update()
        if self.run != False:
            time.sleep(UPDATE_TIME) #should be 0.033
            self.levelTime+=UPDATE_TIME
            self.canvas.itemconfig(self.scoreText,text="Time : %.1f" % self.levelTime)
"""The setup file for generating executable. Run this as standalone from console: python setup.py build"""

import sys
from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need fine tuning.
build_exe_options = {"packages": ["os"], "excludes": ["tkinter"]}

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None
if sys.platform == "win32":
    base = "Win32GUI"

setup(  name = "PrakeOut",
        version = "1.0.0",
        description = "BrakeOut with Editor",
        options = {"build_exe": build_exe_options},
        executables = [Executable("GameHandler.py", base=base)])
﻿from HitDetectionBox import *

class MakePaddle(object):

    PADDLE_WIDTH = 80
    PADDLE_HEIGHT = 20
    PADDLE_SPEED = 16   #should be 16

    def moveLeft(self,evt):
        self.hitbox.xSpeed = -self.hitbox.abs_x_speed
        checkLeft = self.hitbox.checkLeftScreen(0,canvas = self.canvas)

        if checkLeft == "EQUAL":
            self.game.moveBallWithPaddle()
            self.hitbox.stop()
            return
        elif checkLeft == "OVERLAP":
            self.game.moveBallWithPaddle()
            self.hitbox.stop()
            return

        self.hitbox.move()
        self.canvas.move(self.id,self.hitbox.xSpeed,self.hitbox.ySpeed)
        self.hitbox.stop()
        self.game.moveBallWithPaddle()

    def moveRight(self,evt):
        self.hitbox.xSpeed = self.hitbox.abs_x_speed
        checkRight = self.hitbox.checkRightScreen(self.screen_width,canvas = self.canvas)

        if checkRight == "EQUAL":
            self.game.moveBallWithPaddle()
            self.hitbox.stop()
            return
        elif checkRight == "OVERLAP":
            self.game.moveBallWithPaddle()
            self.hitbox.stop()
            return

        self.hitbox.move()
        self.canvas.move(self.id,self.hitbox.xSpeed,self.hitbox.ySpeed)
        self.hitbox.stop()
        self.game.moveBallWithPaddle()

    def moveUp(self,event):
        """ Not in use"""
        self.hitbox.ySpeed = -self.hitbox.abs_y_speed
        checkTop = self.hitbox.checkTopScreen(self.screen_height-300,canvas = self.canvas)      #should be -100

        if checkTop == "EQUAL":
            self.hitbox.stop()
            return
        elif checkTop == "OVERLAP":
            self.hitbox.stop()
            return

        self.hitbox.move()
        self.canvas.move(self.id,self.hitbox.xSpeed,self.hitbox.ySpeed)
        self.hitbox.stop()

    def moveDown(self,evt):
        """ Not in use"""
        self.hitbox.ySpeed = self.hitbox.abs_y_speed
        checkBottom = self.hitbox.checkBottomScreen(self.screen_height,canvas = self.canvas)

        if checkBottom == "EQUAL":
            self.hitbox.stop()
            return
        elif checkBottom == "OVERLAP":
            self.hitbox.stop()
            return

        self.hitbox.move()
        self.canvas.move(self.id,self.hitbox.xSpeed,self.hitbox.ySpeed)
        self.hitbox.stop()




    def __init__(self,canvas,color,x,y, screen_width,screen_height,game):
        self.game = game
        self.canvas = canvas
        self.color = color
        self.state="catch"     #normal and catch. when catch you will catch the ball
        self.screen_width=screen_width
        self.screen_height=screen_height
        self.hitbox = HitBox(x,y,MakePaddle.PADDLE_WIDTH,MakePaddle.PADDLE_HEIGHT,self,abs_x_speed=MakePaddle.PADDLE_SPEED,abs_y_speed=MakePaddle.PADDLE_SPEED,identifier="Paddle")
        self.id = canvas.create_rectangle(self.hitbox.x,self.hitbox.y,self.hitbox.width+self.hitbox.x,self.hitbox.height+self.hitbox.y,fill=self.color);
        self.canvas.bind_all("<Left>",self.moveLeft)
        self.canvas.bind_all("<Right>",self.moveRight)
        #self.canvas.bind_all("<Up>",self.moveUp)
        #self.canvas.bind_all("<Down>",self.moveDown)
        self.canvas.bind_all("<space>",self.spacePress)

    def spacePress(self,evt):
        if self.state == "normal":
            self.canvas.itemconfig(self.id,fill="cyan")
            self.state="catch"
        else:
            self.canvas.itemconfig(self.id,fill="blue")
            self.state="normal"
            self.game.releaseBall()
        print("space press")
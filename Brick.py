from Actors import Actor

class Brick(Actor):
    """Parent class for all the different brick types"""

    def __init__(self,id,canvas,hitbox):
        Actor.__init__(self,id)
        self.hitbox = hitbox
        self.canvas = canvas
